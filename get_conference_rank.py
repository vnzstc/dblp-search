import requests
from bs4 import BeautifulSoup

# Define venue rank order
VENUE_RANKS = {
    "Missing": 0,
    "A*": 1,
    "A": 2,
    "B": 3,
    "C": 4,
}
    
def get_conference_rank(venue_code:str) -> str:
    # URL template with a placeholder for the acronym
    url_template = "https://portal.core.edu.au/conf-ranks/?search={}&by=acronym&source=CORE2023&sort=arank&page=1"
    # Format the URL with the provided acronym
    url = url_template.format(venue_code)
    print(url)
    # Make a GET request to the URL
    response = requests.get(url)

    # Check if the request was successful
    if response.status_code == 200:
        # Parse the response content
        soup = BeautifulSoup(response.content, 'html.parser')

        # Find the table (adjust class if necessary)
        table = soup.find('table')  # Replace 'table-class' with actual class name

        if table:
            # Extract table rows
            rows = table.find_all('tr')

            # Iterate through rows and find the rank
            for row in rows[1:]:
                columns = row.find_all('td')
                acronym_found = columns[1].text.strip()
                rank = columns[3].text.strip()
                # Check if the acronym matches the search
                if acronym_found.lower() == venue_code.lower():
                    if rank in VENUE_RANKS.keys():
                        return rank
                    else:
                        return list(VENUE_RANKS.keys())[0]
        else: print(f"No rank found for conference {venue_code}")
    else:
        print(f"Failed to retrieve conference rank data: {response.status_code}")
    return list(VENUE_RANKS.keys())[0]    

def get_journal_rank(venue_title:str) -> str:
    url_template = "https://portal.core.edu.au/jnl-ranks/?search={}&by=title&source=all&sort=atitle&page=1"
    url = url_template.format('+'.join(venue_title))
    # Make a GET request to the URL
    response = requests.get(url)
    # Check if the request was successful
    if response.status_code == 200:
        # Parse the response content
        soup = BeautifulSoup(response.content, 'html.parser')
        # Find the table (adjust class if necessary)
        table = soup.find('table')  # Replace 'table-class' with actual class name
        if table:
            # Extract table rows
            rows = table.find_all('tr')
            # Iterate through rows and find the rank
            for row in rows[1:]:
                columns = row.find_all('td')
                title_found = columns[0].text.strip()
                rank = columns[3].text.strip()
                # Check if the acronym matches the search
                if title_found.lower() == venue_title.lower():
                    if rank in VENUE_RANKS.keys():
                        return rank
                    else:
                        return list(VENUE_RANKS.keys())[0]
        else: print(f"No rank found for journal: {venue_title}")
    else:
        print(f"Failed to retrieve journal rank data: {response.status_code}")
    return list(VENUE_RANKS.keys())[0]
